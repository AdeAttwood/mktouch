#!/bin/bash

SRC_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)";

DEBUG_DIR="$SRC_DIR/build/Debug";
RELEASE_DIR="$SRC_DIR/build/Release";

case "$1" in
    apt-pkg)
        ldd build/Release/mktouch | awk '/=>/{print $(NF-1)}' | while read n; do dpkg-query -S $n | cut -d ":" -f 1; done | uniq | sort > apt-pkg.txt;
        ;;
    debug)
        echo "CMAKE --> Debug";
        mkdir -p "$DEBUG_DIR";
        cd "$DEBUG_DIR";
        BUILD_TYPE="Debug";
        BUILD="true";
        MAKE="true";
        ;;
    release)
        echo "CMAKE --> Release";
        mkdir -p "$RELEASE_DIR";
        cd "$RELEASE_DIR";
        BUILD_TYPE="Release";
        BUILD="true";
        MAKE="true";
        ;;
    package)
        echo "CMAKE --> Release";
        mkdir -p "$RELEASE_DIR";
        cd "$RELEASE_DIR";
        BUILD_TYPE="Release";
        BUILD="true";
        MAKE="true";
        PACKAGE="true";
        ;;
    clean)
        echo "CMAKE --> Cleaning";
        rm -rf "$SRC_DIR/build";
        ;;
    *) 
        echo "CMAKE --> Build not found";
        echo " -- apt-pkg";
        echo " -- debug";
        echo " -- release";
        echo " -- clean";
        exit 0;
       ;;
esac

if [ -n "$BUILD" ]; then
    cp -R $SRC_DIR/man $(pwd);
    cmake -DCMAKE_BUILD_TYPE=$BUILD_TYPE $SRC_DIR;
fi

if [ -n "$MAKE" ]; then
    echo "CMAKE --> Make";
    make;
fi

if [ -n "$PACKAGE" ]; then
    echo "CMAKE --> Package";
    make package;
fi
