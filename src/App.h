#ifndef APP_H
#define APP_H

#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

namespace po = boost::program_options;

class App {
    po::variables_map vm;
    po::options_description desc;
    po::positional_options_description positionalOptions;
    
    public:
        std::string version = "1.0.1";
        std::string name = "mktouch";
        
        App();
        App(const App& orig);
        virtual ~App();      

        void parseArgs(int argc, char** argv);
        
        void showHelp();
        void showVersion();
        bool hasOption(std::string name);
        int shutDown(int code);
 
        po::variable_value getOption(std::string name);

    private:
        std::vector<std::string> _getUnlimitedPositionalArgs(const po::positional_options_description& p);
};

#endif /* APP_H */

