#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include "App.h"
#include "Cli.h"

App::App() {}
App::App(const App& orig) {}
App::~App() {}

int App::shutDown(int code) {
    return code;
}

void App::parseArgs(int argc, char** argv) {
    namespace po = boost::program_options;
    
    po::options_description options("Options");
    options.add_options()
        ("force,f", "Force create the file even if it exsits")
        ("help,h", "Show this messages")
        ("version,V", "Display the version number");

    po::options_description arguments("Arguments");
    arguments.add_options()
        ("file-path", po::value<std::string>(), "The path where to create the file")
        ("file", po::value<std::string>(), "The file to create");

    positionalOptions.add("file-path", 1);
    positionalOptions.add("file", 1);

    desc.add(arguments).add(options);

    po::store(po::command_line_parser(argc, argv).
                positional(positionalOptions).
                options(desc).
                run(),
                vm);

     po::notify(vm);
}

void App::showHelp() {
    std::vector<std::string> parts;
    
    parts.push_back("Usage: ");
    parts.push_back(this->name);
    
    size_t N = positionalOptions.max_total_count();
    
    if (N == std::numeric_limits<unsigned>::max()) {
        std::vector<std::string> args = this->_getUnlimitedPositionalArgs(positionalOptions);
        parts.insert(parts.end(), args.begin(), args.end());
    } else {
        for(size_t i = 0; i < N; ++i) {
            parts.push_back(positionalOptions.name_for_position(i));
        }
    }
    
    if (desc.options().size() > 0) {
        parts.push_back("[options]");
    }
    
    std::ostringstream oss;
    std::copy(parts.begin(), parts.end(), std::ostream_iterator<std::string>(oss, " "));
    oss << '\n' << desc;
    
    std::cout << Cli::style(oss.str());
}

std::vector<std::string> App::_getUnlimitedPositionalArgs(const po::positional_options_description& p)
{
    assert(p.max_total_count() == std::numeric_limits<unsigned>::max());

    std::vector<std::string> parts;

    const int MAX = 1000; 
    std::string last = p.name_for_position(MAX);

    for (size_t i = 0; true; ++i) {
        std::string cur = p.name_for_position(i);
        if (cur == last) {
            parts.push_back(cur);
            parts.push_back('[' + cur + ']');
            parts.push_back("...");
            return parts;
        }
        parts.push_back(cur);
    }
    return parts; // never get here
}

void App::showVersion() {
    std::cout << "Version: " << this->version << std::endl;
}

bool App::hasOption(std::string name) {
    return vm.count(name);
}

po::variable_value App::getOption(std::string name) {
    return vm[name];
}

