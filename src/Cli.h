
#ifndef CLI_H
#define CLI_H

#include <iostream>
#include <string>
#include <map>

class Cli {
public:
    Cli();
    Cli(const Cli& orig);
    virtual ~Cli();
    
    static void out(std::string output);
    static std::string in();
    static void hideCursor();
    static void showCursor();
    static void clearScreen();
    static void clearLine();

    static std::string readLine(std::string question);
    static int askQuestion(std::string qustion, std::map < int, std::string >);
    static std::string style(std::string string);
private:
    static bool replace(std::string& str, const std::string& from, const std::string& to);

};

#endif /* CLI_H */

