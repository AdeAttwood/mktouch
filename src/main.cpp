#include <iostream> 
#include <string>
#include <map>

#include <boost/filesystem.hpp>

#include "App.h"
#include "Cli.h"

namespace fs = boost::filesystem;

namespace { 
  const size_t SUCCESS         = 0; 
  const size_t ERROR           = 1; 
  const size_t UNHANDLED_ERROR = 2; 
} // namespace 

int main(int argc, char** argv) {
    App app;

    try {
        app.parseArgs(argc, argv);
    } catch(std::exception& E) {
        std::cout << "ERROR: " << E.what() << std::endl;
        return app.shutDown(UNHANDLED_ERROR);
    }
        
    if (app.hasOption("help")) {
        app.showHelp();
        return app.shutDown(SUCCESS);
    }
    
    if (app.hasOption("version")) {
        app.showVersion();
        return app.shutDown(SUCCESS);
    }

    if (!app.hasOption("file-path")) {
        app.showHelp();
        return app.shutDown(ERROR);
    }

    std::string filePath = app.getOption("file-path").as<std::string>();
    fs::create_directories(filePath);

    if (app.hasOption("file")) {
        std::string file = filePath + "/" + app.getOption("file").as<std::string>();
        
        if (!app.hasOption("force") && fs::exists(file)) {
            std::cout << "ERROR: " << file << " already exists" << std::endl;
            return app.shutDown(ERROR);
        }
        
        std::ofstream outfile (file);
        outfile.close();
    }

    return app.shutDown(SUCCESS);
};
