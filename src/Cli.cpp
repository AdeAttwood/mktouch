#include <iostream>
#include <string>
#include <map>

#include "Cli.h"

Cli::Cli() {}
Cli::Cli(const Cli& orig) {}
Cli::~Cli() {}

std::string Cli::style(std::string string) {
    
    Cli::replace(string, "{{eol}}", "\n");
    
    Cli::replace(string, "{{", "\e[");
    Cli::replace(string, "}}", "m");
     
    Cli::replace(string, "cli_fg_red", "31");
    Cli::replace(string, "cli_fg_default", "39");
    Cli::replace(string, "cli_fg_green", "32");
    Cli::replace(string, "cli_fg_yellow", "33");
    Cli::replace(string, "cli_fg_blue", "34");
    Cli::replace(string, "cli_fg_white", "97");
    Cli::replace(string, "cli_fg_black", "30");
    
    Cli::replace(string, "cli_bg_red", "41");
    Cli::replace(string, "cli_bg_default", "49");
    Cli::replace(string, "cli_bg_green", "42");
    Cli::replace(string, "cli_bg_yellow", "43");
    Cli::replace(string, "cli_bg_blue", "44");
    Cli::replace(string, "cli_bg_white", "107");
    Cli::replace(string, "cli_bg_black", "40");
    
    Cli::replace(string, "cli_reset_normal", "0");
    Cli::replace(string, "cli_reset_bold", "21");
    Cli::replace(string, "cli_reset_dim", "22");
    Cli::replace(string, "cli_reset_underline", "24");
    Cli::replace(string, "cli_reset_blink", "25");
    Cli::replace(string, "cli_reset_invert", "27");
    Cli::replace(string, "cli_reset_hidden", "28");

    Cli::replace(string, "cli_normal", "0");
    Cli::replace(string, "cli_bold", "1");
    Cli::replace(string, "cli_dim", "2");
    Cli::replace(string, "cli_underline", "4");
    Cli::replace(string, "cli_blink", "5");
    Cli::replace(string, "cli_invert", "7");
    Cli::replace(string, "cli_hidden", "8");

    return string;
}

bool Cli::replace(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty()) {
        return false;
    }

    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }

    return true;
}

void Cli::out(std::string output) {
    std::cout << Cli::style(output);
}

void Cli::hideCursor() {
    Cli::out("\033[?25l");
}

void Cli::showCursor() {
    Cli::out("\033[?25h");
}

void Cli::clearScreen() {
    Cli::out("\033[2J");
}

void Cli::clearLine() {
    Cli::out("\033[2K");
}

std::string Cli::in() {
    std::string out;
    std::cin >> out;
    return out;
}

std::string Cli::readLine(std::string question) {
    Cli::out(question);
    return Cli::in();
}

int Cli::askQuestion(std::string question, std::map <int, std::string> questionOptions) {
    question += "{{eol}}";

    for (const auto& kv : questionOptions) {
        question += "[" + std::to_string(kv.first) + "] " + kv.second + "{{eol}}";
    }


    std::string out;
    
    while (out.empty()) {
        std::string answer = Cli::readLine(question);

        if (questionOptions.find(std::stoi(answer)) == questionOptions.end()) {
            Cli::out("Invalid option{{eol}}");
        } else {
            out = answer;
        }
    }

    return std::stoi(out);
}